package com.instanceof42.study.hibernate.usertype;

import com.instanceof42.study.hibernate.usertype.entity.Book;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.time.Instant;

import static org.junit.Assert.*;

public class BookJpaTest {

    private EntityManager entityManager;


    @Before
    public void setup() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory( "test" );
        this.entityManager = entityManagerFactory.createEntityManager();
        this.entityManager.getTransaction().begin();
    }

    @After
    public void tearDown() {
        this.entityManager.getTransaction().commit();
        this.entityManager.close();
    }


    @Test
    public void test() {

        Instant now = Instant.now();
        Book book = new Book(1L, "hitchhiker's guide to the galaxy", now);
        entityManager.persist(book);
        this.entityManager.getTransaction().commit();
        entityManager.detach(book);

        this.entityManager.getTransaction().begin();
        Book bookFromDatabase = entityManager.find(Book.class, 1L);
        assertEquals(now, bookFromDatabase.getPublishDate());

    }


}