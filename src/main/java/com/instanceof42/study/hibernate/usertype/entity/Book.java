package com.instanceof42.study.hibernate.usertype.entity;

import com.instanceof42.study.hibernate.usertype.InstantConverter;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.Instant;

@Entity
public class Book {

    @Id
    private Long id;

    private String name;

    @Convert(converter = InstantConverter.class)
    private Instant publishDate;

    public Book() {

    }

    public Book(Long id, String name, Instant publishDate) {
        this.id = id;
        this.name = name;
        this.publishDate = publishDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Instant publishDate) {
        this.publishDate = publishDate;
    }
}
