package com.instanceof42.study.hibernate.usertype;

import javax.persistence.AttributeConverter;
import java.sql.Timestamp;
import java.time.Instant;

public class InstantConverter implements AttributeConverter<Instant, Timestamp> {

    @Override
    public Timestamp convertToDatabaseColumn(Instant attribute) {
        return attribute != null ? Timestamp.from(attribute) : null;
    }

    @Override
    public Instant convertToEntityAttribute(Timestamp dbData) {
        return dbData != null ? dbData.toInstant() : null;
    }
}
